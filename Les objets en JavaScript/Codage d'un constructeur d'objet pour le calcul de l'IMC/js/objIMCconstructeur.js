/*

Name : objIMCconstructeur.js
Author : Hanni Molabaccus 
Date : 08/04/2021

*/
// Définition de la fonction constructeur
function Patient(prmNom,prmPrenom,prmSexe,prmAge,prmTailles,prmPoids) {
    this.nom = prmNom;
    this.prenom = prmPrenom;
    this.sexe = prmSexe;
    this.age = prmAge ;
    this.tailles = prmTailles
    this.poids = prmPoids;
    this.decrire = function() {
        let description;
        description = "Le patient " + this.prenom + " " + this.nom + " de sexe " + this.sexe +  " est agé de " + this.age + "ans. Il mesure " + this.tailles + "cm et pèse " + this.poids + "kg";
        return description;
    };
    this.IMC = function () {
        let IMC;
        IMC = this.poids / ((this.tailles / 100) * (this.tailles / 100));
        return IMC.toFixed(2);
    };
    this.corpulence = function () {
        IMC = this.IMC(); // récupére l'imc
        let interpretation = "";
        // Verifie le sexe puis interprete l'IMC en fonction
        if (this.sexe == 'féminin') {
            if (IMC < 16.5) {
                interpretation = "Dénutrition";
            }
            else if (IMC < 18.5) {
                interpretation = "Maigreur";
            }
            else if (IMC < 25) {
                interpretation = "Corpulence normale";
            }
            else if (IMC < 30) {
                interpretation = "Surpoids";
            }
            else if (IMC < 35) {
                interpretation = "Obésité modérée";
            }
            else if (IMC < 40) {
                interpretation = "Obésité sévère";
            }
            else {
                interpretation = " Obésité morbide";
            }
        }
        else {
            if (IMC < 18.5) {
                interpretation = "Dénutrition";
            }
            else if (IMC < 20.5) {
                interpretation = "Maigreur";
            }
            else if (IMC < 27) {
                interpretation = "Corpulence normale";
            }
            else if (IMC < 32) {
                interpretation = "Surpoids";
            }
            else if (IMC < 37) {
                interpretation = "Obésité modérée";
            }
            else if (IMC < 42) {
                interpretation = "Obésité sévère";
            }
            else {
                interpretation = " Obésité morbide";
            }
        }
        return interpretation;
    };
}

// création des patients
let objPersonne1 = new Patient('Dupond', 'Jean', "masculin",30, 180, 85) ;
let objPersonne2 = new Patient('Moulin', 'Isabelle', "féminin", 46, 158, 74);
let objPersonne3 = new Patient('Martin', 'Eric', 'masculin', 42, 165, 90);

// affichage de la description des objets
 
console.log(objPersonne1.decrire());
console.log("Son IMC est de : " + objPersonne1.IMC());
console.log("Il est en situtation de " + objPersonne1.corpulence());

console.log(objPersonne2.decrire());
console.log("Son IMC est de : " + objPersonne2.IMC());
console.log("Il est en situtation de " + objPersonne2.corpulence());

console.log(objPersonne3.decrire());
console.log("Son IMC est de : " + objPersonne3.IMC());
console.log("Il est en situtation de " + objPersonne3.corpulence());