/*

Name : imbrique.js
Author : Hanni Molabaccus 
Date : 08/04/2021

*/
let objPersonne = {
    nom: 'Dupond',
    prenom: 'Jean',
    age: 30,
    sexe: 'masculin',
    taille: '180',
    poids: '85',
    decrire: function () { // Résume toutes les infos de l'objPersonne et le renvoie
        let description;
        description = "Le patient " + this['prenom'] + " " + this['nom'] + " de sexe " + this['sexe'] + " est agé de " + this['age'] + "ans. Il mesure " + this['taille'] + " et pèse " + this['poids'] + " kg";
        return description;
    },
    calculer_IMC: function () { // Fonction qui Calcul l'imc en fonction des proprièté poids et tailles
        let IMC;
        IMC = this['poids'] / ((this['taille'] / 100) * (this['taille'] / 100));
        return IMC.toFixed(2);
    },
    interpreter_IMC: function () { // Fonction qui interprete l'imc en fonction de sa valeur
        IMC = this.calculer_IMC(); // récupère la proprièté IMC précédement ajouté
        let interpretation = "";
        if (IMC < 16.5) {
            interpretation = "Dénutrition";
        }
        else if (IMC < 18.5) {
            interpretation = "Maigreur";
        }
        else if (IMC < 25) {
            interpretation = "Corpulence normale";
        }
        else if (IMC < 30) {
            interpretation = "Surpoids";
        }
        else if (IMC < 35) {
            interpretation = "Obésité modérée";
        }
        else if (IMC < 40) {
            interpretation = "Obésité sévère";
        }
        else {
            interpretation = " Obésité morbide";
        }
        return interpretation;
    }
};

console.log(objPersonne.decrire()); // appel la proprièté décrire de l'objPersonne
console.log("Son IMC est de: " + objPersonne.calculer_IMC()); // Récupère l'IMC
console.log("Il est en situation de " + objPersonne.interpreter_IMC()); // Interpréte l'IMC
