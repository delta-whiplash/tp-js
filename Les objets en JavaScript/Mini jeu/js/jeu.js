function Personnage(prmNom,prmNiveau){
    this.nom = prmNom;
    this.niveau = prmNiveau;
    this.arme = "";
}

Personnage.prototype.saluer = function(){
    return this.nom+" vous salue!!!";
}

function Gerrier(prmNom,prmNiveau,prmArme){
    Personnage.call(this,prmNom,prmNiveau);
    this.arme = prmArme;
}
Gerrier.prototype = Object.create(Personnage.prototype);
Gerrier.prototype.Combattre = function(){
    return this.nom+" est un guerrier qui se bat avec "+this.arme;
}


function Magicien(prmNom,prmNiveau,prmPouvoir){
    Personnage.call(this,prmNom,prmNiveau);
    this.pouvoir = prmPouvoir;
}
Magicien.prototype = Object.create(Personnage.prototype);
Magicien.prototype.posseder = function(){
    return this.nom+" est un magicien qui possède le pouvoir de "+this.pouvoir;
}



let objArthur = new Gerrier("Arthur",3,"épé");
console.log(objArthur.saluer());
console.log(objArthur.Combattre());
let objMerlin = new Magicien("Merlin",2,"prédire les batailles");
console.log(objMerlin.saluer());
console.log(objMerlin.posseder());
