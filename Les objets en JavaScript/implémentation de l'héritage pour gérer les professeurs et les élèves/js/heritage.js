/**
 * @ Author: Hanni Molabaccus
 * @ Create Time: 2021-04-27 10:16:18
 * @ Description:
 */

function Personne(prmNom,prmPrenom,prmAge,prmSexe){
    this.nom = prmNom;
    this.prenom = prmPrenom;
    this.age = prmAge;
    this.sexe = prmSexe;
}
Personne.prototype.decrire = function(){
    let description;
    description = "Cette personne s'appelle " + this.prenom + " " + this.nom + " elle est agée de " + this.age + " ans" ;
    return description;
}

function Professeur(prmNom,prmPrenom,prmAge,prmSexe,prmMatiere){
    Personne.call(this,prmNom, prmPrenom, prmAge, prmSexe);
    this.matiere = prmMatiere;
}
Professeur.prototype = Object.create(Personne.prototype);
Professeur.prototype.decrire_plus = function() {
    let description;
    let prefixe;
    if(this.sexe == 'M') {
        prefixe = 'Mr';
    } else {
        prefixe = 'Mme';
    }
    description = prefixe + " " + this.prenom + " " + this.nom + " est professeur de " + this.matiere;
    return description;
}

function Eleve(prmNom,prmPrenom,prmAge,prmSexe,prmClasse){
    Personne.call(this,prmNom, prmPrenom, prmAge, prmSexe);
    this.classe = prmClasse;
}
Eleve.prototype = Object.create(Personne.prototype);
Eleve.prototype.decrire_plus = function() {
    let description;
    description = this.prenom + " " + this.nom + " est un élève de " + this.classe;
    return description;
}

let objProfesseur1 = new Professeur('Dupond', 'Jean', 30, 'M', 'Mathématiques');
let objEleve = new Eleve('Dutillieul', 'Dorian', 18, 'M', 'SNIR1');
console.log(objProfesseur1.decrire());
console.log(objProfesseur1.decrire_plus());
console.log(objEleve.decrire());
console.log(objEleve.decrire_plus());
