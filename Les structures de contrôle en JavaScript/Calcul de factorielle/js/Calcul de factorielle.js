/*

Name : Calcul de factorielle.js
Author : Hanni Molabaccus 
Date : 07/04/2021

*/
let n = 10;
let factorielle = n;
if (n>1) {
    console.log("Le nombre est strictement supérieur à 1");
    for (let index = 1; index < factorielle; index++) {    
        n = n*index;
    }
    console.log(n);
}
else {
    console.log("Le nombre n'est pas strictement supérieur à 1");
}
