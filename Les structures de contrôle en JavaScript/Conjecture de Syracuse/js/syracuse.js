/*

Name : syracuse.js
Author : Hanni Molabaccus 
Date : 07/04/2021

*/
let N = 14;
let values = ""+N;
let counter = 0;
let maxvalue = N;
console.log("Suite Syracuse pour " + N + ":");
if (N < 1) {
    console.log("Erreur N est inférieur à 0");
}
else {
    while (N > 1) {
        if (N > maxvalue) {
            maxvalue = N;
        }
        if (N %2 == 0) {
            N = N/2;
        }
        else {
            N = (N*3)+1;
        }
        values = values + "-" +N; 
        counter++;
    }
}
console.log(values);
console.log("Temps de vol = " + counter);
console.log("Altitude maximal = " + maxvalue);