/*

Name : conversion.js
Author : Hanni Molabaccus 
Date : 07/04/2021

*/
console.log("1 euro(s) = " + "1.65 dollar(s)");
for (let index = 2; index <= 16384; index = index*2) {
    console.log(index + " euro(s) = " + (index * 1.65).toFixed(2) + " dollar(s)");
}

