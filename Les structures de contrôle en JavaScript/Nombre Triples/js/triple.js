/*

Name : triple.js
Author : Hanni Molabaccus 
Date : 07/04/2021

*/
let n = 2;
let display = "";
console.log("Valeur de départ : 2");
for (let index = 2; index < 12; index++){
    display = display + n+ " ";
    n = n*3;
}
console.log("Valeur de la suite : " + display);