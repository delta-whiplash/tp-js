/*

Name : Fibonacci.js
Author : Hanni Molabaccus 
Date : 07/04/2021

*/
let n1 = 0;
let n2 = 1;
let somme = 0;
let display = "0 " + n2;
for (let i = 2; i < 17; i++) {
    //somme des deux derniers nombres
    somme = n1 + n2;
    //assigner la dernière valeur à la première
    n1 = n2;
    //attribuer la somme au dernier
    n2 = somme;
    display = display +" " + n2;
}
console.log(display);