/*

Name : imc.js
Author : Hanni Molabaccus 
Date : 07/04/2021

*/
console.log("Calcul de l'IMC :");
let taille = 174;
let poids = 54;
let imc = poids / ((taille / 100) * (taille / 100));

console.log("taille : " + taille + "cm");
console.log("poids: " + poids + "kg");
console.log("IMC = " + imc.toFixed(1));
if (imc < 16.5) {
    console.log("Interprétation de l'IMC : Dénutrition");
}
else if (imc < 18.5) {
    console.log("Interprétation de l'IMC : Maigreur");
}
else if (imc < 25) {
    console.log("Interprétation de l'IMC : Corpulence normale");
}
else if (imc < 30) {
    console.log("Interprétation de l'IMC : Surpoids");
}
else if (imc < 35) {
    console.log("Interprétation de l'IMC : Obésité modérée");
}
else if (imc < 40) {
    console.log("Interprétation de l'IMC : Obésité sévère");
}
else {
    console.log("Interprétation de l'IMC : Obésité morbide");
}