/*

Name : Calcul de moyenne.js
Author : Hanni Molabaccus 
Date : 07/04/2021

*/
let listeValeurs = [10,25,41,5,9,11];
let somme =0;
for (let index = 0; index < listeValeurs.length; index++) {
   somme = somme+listeValeurs[index];
}
console.log("Somme des valeurs : " + somme);
console.log("Moyenne des valeurs : " + (somme/listeValeurs.length).toFixed(2));