# TP JS

Mes TP de JS en snir-1 pendant le confinement
> * Auteur : Hanni Molabaccus

## Sommaire


- Introduction
     - [HelloWorld](introduction/HelloWorld%20JS/README.md)
  

- Variable et Opérateurs en JavaScript
  - [Calculer d'IMC](Variables%20et%20Opérateurs%20en%20JavaScript/calcul%20d'IMC/README.md)
  - [Calcul d'une Surface](Variables%20et%20Opérateurs%20en%20JavaScript/../Variables%20et%20Opérateurs%20en%20JavaScript/Calcul%20d'une%20surface/README.md)
  - [conversion CelciusFahrenheit](Variables%20et%20Opérateurs%20en%20JavaScript/conversion%20CelciusFahrenheit/README.md)


- Les structures de contrôle en JavaScript
  - [Interprétation de l'IMC](Les%20structures%20de%20contrôle%20en%20JavaScript/calcul%20d'IMC/README.md)
  - [Conjecture de Syracuse](Les%20structures%20de%20contrôle%20en%20JavaScript/Conjecture%20de%20Syracuse/README.md)
  - [Calcul de factorielle](Les%20structures%20de%20contrôle%20en%20JavaScript/Calcul%20de%20factorielle/README.md)   
  - [Conversion EurosDollars](Les%20structures%20de%20contrôle%20en%20JavaScript/Conversion%20EurosDollars/README.md)
  - [Nombre 20Triples](Les%20structures%20de%20contrôle%20en%20JavaScript/Nombre%20Triples/README.md)
  - [Suite de Fibonacci](Les%20structures%20de%20contrôle%20en%20JavaScript/Suite%20de%20Fibonacci/README.md)
  - [Table de Multiplication](Les%20structures%20de%20contrôle%20en%20JavaScript/Table%20de%20Multiplication/README.md)


- Fonctions
  - [Fonctions pour calculer et interpréter l'IMC](les%20fonctions/fonctions%20pour%20calculer%20et%20interpréter%20l'IMC/README.md)
  - [Fonctions Calculer temps du temps d'un trajet](les%20fonctions/calcul%20du%20temps%20de%20parcours%20d'un%20trajet/README.md)
  - [Recherches du nombre de multiples de 3](les%20fonctions/recherche%20du%20nombre%20de%20multiples%20de%203/README.md)