/*

Name : imc.js
Author : Hanni Molabaccus 
Date : 06/04/2021

*/
console.log("Calcul de l'IMC :");
let taille = 160;
let poids = 100;
let imc = poids / ((taille / 100) * (taille / 100));

console.log("taille : " + taille + "cm");
console.log("poids: " + poids + "kg");
console.log("IMC = " + imc.toFixed(1));