/*

Name : Celcius2fahrenheit.js
Author : Hanni Molabaccus 
Date : 06/04/2021

*/
console.log("Conversion Celsius (°C) / Fahrenheit (°F):");
let TemperatureEnCelsius = 22.6;
let TemperatureEnFahrenheit = TemperatureEnCelsius * 1.8 + 32;

console.log("une température : " + TemperatureEnCelsius + "°C correspond à une température de " + TemperatureEnFahrenheit.toFixed(1) + "°F");