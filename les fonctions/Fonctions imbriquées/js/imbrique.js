/*

Name : imbrique.js
Author : Hanni Molabaccus 
Date : 08/04/2021

*/
function calculerSurface() {
    // déclaration des variables locales
    let longueur;
    let largeur;
    // déclaration des fonctions internes
    function initialiserLongueur() {        
            longueur = 4;
    }
    function initialiserLargeur() {
            largeur = 3;
    }
    // appel des fonctions internes
    initialiserLongueur();
    initialiserLargeur();
    // retour du résultat
    return largeur * longueur;
}

let surface = calculerSurface();
console.log(surface);
