/*

Name : parcours.js
Author : Hanni Molabaccus 
Date : 08/04/2021

*/
function calculerTempsParcoursSec(prmVitesse,prmDistance) {
    let returnvalue = prmDistance/prmVitesse*3600;
    return returnvalue;
}
function convertir_h_min_sec(prmtempensec) {
    let heures;
    let minutes;
    let secondes;
    let returnvalue;
    heures = Math.floor(prmtempensec/3600);
    minutes = Math.floor((prmtempensec/60) - (heures*60));
    secondes = Math.floor(prmtempensec- (minutes*60) - (heures*3600))
    returnvalue = heures + "h " + minutes + "min " + secondes + "s";
    return returnvalue;
}
let vitesse = 90;
let distance = 500; 
console.log("Calcul du temps de trajet");
console.log("Vitesse moyenne (en km/h):     "+vitesse);
console.log("Distance (en km):      "+distance);
console.log("A " + vitesse + " km/h, une distance de " + distance + " km est parcourue en " + calculerTempsParcoursSec(vitesse,distance) + " s");
tempensec = calculerTempsParcoursSec(vitesse,distance);
console.log("A " + vitesse + " km/h, une distance de " + distance + " km est parcourue en " + convertir_h_min_sec(tempensec));
