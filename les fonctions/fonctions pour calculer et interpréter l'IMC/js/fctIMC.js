/*

Name : fctIMC.js
Author : Hanni Molabaccus 
Date : 08/04/2021

*/
function calculerIMC(prmTaille,prmPoids) {
    let valIMC;
    valIMC = prmPoids / ((prmTaille / 100) * (prmTaille / 100));
    return valIMC;
}

function interpreterIMC(prmIMC) {
    let interpretation = "";
    if (prmIMC < 16.5) {
        interpretation = "Dénutrition";
    }
    else if (prmIMC < 18.5) {
        interpretation = "Maigreur";
    }
    else if (prmIMC < 25) {
        interpretation = "Corpulence normale";
    }
    else if (prmIMC < 30) {
        interpretation = "Surpoids";
    }
    else if (prmIMC < 35) {
        interpretation = "Obésité modérée";
    }
    else if (prmIMC < 40) {
        interpretation = "Obésité sévère";
    }
    else {
        interpretation = " Obésité morbide";
    }
    return interpretation;
}
let IMC = calculerIMC(174,54);
console.log("Votre IMC est de : " + IMC.toFixed(1) + "\nVous etes en " + interpreterIMC(IMC));