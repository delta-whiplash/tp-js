/*

Name : multiples3.js
Author : Hanni Molabaccus 
Date : 08/04/2021

*/
function rechercher_Mult3(prmmax) {
    let returnvalue = "0";
    for (let index = 1; index < prmmax; index++) {
        if (index%3 == 0){
            returnvalue = returnvalue + "-" + index ;
        }
    }
    return returnvalue;
}
console.log("Recherches des multiples de 3 :");
let valeurlimites = 20;
console.log("Multiples de 3 : " + rechercher_Mult3(valeurlimites));